import requests
from db.base import engine
from db.models import Offer, Contact, OfferDetails, Geo, City
from parsers.address_parser import parse_address
from parsers.automation_olx import parse_contacts
from utils import exceptions as exc
from db.enums import Currency, Heating, Category
import funcy as f
from utils.export import export_to_xml
from utils.log import get_logger
from sqlalchemy.orm import Session
import os
from dotenv import load_dotenv
import time
from parsers.address_parser import parse_address
from Levenshtein import distance as lev
import pandas as pd


load_dotenv()


logger = get_logger('api_parser')


def get_olx_api(url):
    resp = requests.get(url)
    if resp.status_code == 200:
        return resp.json()
    raise exc.OlxApiNotResponded(resp.status_code, resp.json()['error'])


def get_param(params, key):
    try:
        return next(param for param in params if param.get('key') == key)
    except Exception:
        return {'value': {'key': None, 'label': ''}}


def create_contact(name):
    with Session(engine) as session:
        row = Contact(name=name)
        session.add(row)
        session.commit()
        session.refresh(row)
        return row


def get_images(photos):
    return [
        photo['link'].format(width=photo['width'], height=photo['height'])
        for photo in photos
    ]


def get_heating(heating):
    if heating:
        try:
            return getattr(Heating, heating.replace('-', '_')).value
        except AttributeError:
            return Heating.other.value


def create_offer_details(params):
    eject_param = f.partial(get_param, params)
    floor = round(float(eject_param('floor')['value']['key']))
    total_floors = round(float(eject_param('total_floors')['value']['key']))
    with Session(engine) as session:
        row = OfferDetails(
            rooms=(
                eject_param('number_of_rooms_string')['value']['label'].
                split()[0].strip('+')
            ),
            area=eject_param('total_area')['value']['key'],
            total_floors=total_floors,
            floor=floor if total_floors >= floor else None,
            living_complex=eject_param('zkh')['value']['label'],
            heating=get_heating(eject_param('heating')['value']['key']),
        )
        session.add(row)
        session.commit()
        session.refresh(row)
        return row


def get_city(session, title):
    title = title.replace("'", '`')
    stmt = session.query(City).filter(City.title_alt.contains(title)).statement
    try:
        df = pd.read_sql_query(stmt, session.bind)
        df['similarity'] = df['title_alt'].apply(lambda x: lev(x, title))
        city = df.sort_values(by='similarity').iloc[0]
        return int(city['id'])
    except Exception as e:
        logger.error(e)


def create_geo(data):
    with Session(engine) as session:
        row = Geo(
            city_id=get_city(session, data['location']['city']['name']),
            region=data['location']['region']['name'],
            address=(
                parse_address(data['description']) or
                parse_address(data['title'])
            ),
            lat=data['map']['lat'],
            lon=data['map']['lon'],
        )
        session.add(row)
        session.commit()
        session.refresh(row)
        return row


def parse_offer(data, category_id):
    contact = create_contact(data['contact']['name'])
    params = create_offer_details(data['params'])
    with Session(engine) as session:
        row = Offer(
            olx_id=data['id'],
            url=data['url'],
            title=data['title'],
            descr=data['description'],
            category_id=category_id,
            price=(
                price := get_param(data['params'], 'price')
            )['value']['value'],
            currency=getattr(Currency, price['value']['currency']).value,
            contact_id=contact.id,
            images='\n'.join(get_images(data['photos'])),
            params_id=params.id,
            geo_id=create_geo(data).id,
        )
        session.add(row)
        session.commit()
        session.refresh(row)
    return row


def parse_api_olx(url, category):
    offers = get_olx_api(url)
    rows = []
    for offer in offers['data']:
        with Session(engine) as session:
            if session.query(Offer).filter_by(olx_id=offer['id']).first():
                continue
        rows.append(parse_offer(offer, category))

    return rows


def rotate_api_url(category, url):
    offers = []
    for offset in range(0, 51, 50):
        time.sleep(2)
        offers.extend(parse_api_olx(
            url.format(offset=offset), getattr(Category, category).value
        ))
    return offers


def parse_all_api_urls():
    urls = {
        'sell': os.environ['SELL_API_URL'],
        'rent': os.environ['RENT_API_URL']
    }
    for category, url in urls.items():
        offers = rotate_api_url(category, url)
        try:
            parse_contacts(offers)
        except Exception as e:
            logger.error(e)
        export_to_xml(offers)
