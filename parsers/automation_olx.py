import datetime
from sqlalchemy.orm import Session
from db.base import engine
from playwright.sync_api import sync_playwright
from db.models import Account
from utils.log import get_logger
import json
from playwright_stealth import stealth_sync
from playwright._impl._api_types import TimeoutError as PWTimeoutError
from utils import exceptions as exc
import signal
import os


logger = get_logger(__name__)
PHONE_BUTTON_SEL = 'button[data-cy="ad-contact-phone"]'
OTHER_BUTTON_SEL = 'div[data-testid="other-contacts"]'
PROMPT_MESSAGE_SEL = 'p[data-testid="prompt-message"]'
LOGIN_BUTTON_SEL = 'button[data-testid="login-button"]'
CAPTCHA_SEL = 'iframe[title="reCAPTCHA"]'


def parse_contacts(offers):
    for offer in offers:
        with Session(engine) as session:
            session.add(offer)
            if offer.contact.phones:
                return
            account = session.query(Account).filter(
                Account.captched_until <= datetime.datetime.now()
            ).order_by(Account.last_used).first()

            try:
                if phone := parse_with_browser(offer, account):
                    offer.contact.phones = phone
                    logger.info(
                        'Contact from offer {} saved'.format(offer.olx_id)
                    )
            except exc.CaptchaFound:
                account.captched_until = (
                    datetime.datetime.now() + datetime.timedelta(hours=2)
                )
            if account:
                account.last_used = datetime.datetime.now()
            session.commit()


def parse_with_browser(offer, account):
    with sync_playwright() as pw:
        browser = pw.chromium.launch(headless=True, slow_mo=300)
        page = make_page(browser, account.proxy if account else '')
        try:
            goto(page, offer.url)
            if is_phone_protected(page) and account:
                if not account.cookies:
                    login_with_acc(page, account)
                    save_cookies(page, account)
                    logger.info('Logined {}'.format(account.login))
                else:
                    page.context.add_cookies(json.loads(account.cookies))
                    logger.info('Cookies loaded {}'.format(account.login))
                    page.reload()
                page.wait_for_load_state('domcontentloaded', timeout=60000)
                goto(page, offer.url)
            return get_phone(page)
        except exc.CaptchaFound:
            raise exc.CaptchaFound()
        except Exception as e:
            logger.error(e)
        finally:
            page.close()
            browser.close()
            signal.signal(signal.SIGCHLD, handle_sigchld)


def handle_sigchld(signum, frame):
    while True:
        try:
            pid, status = os.waitpid(-1, os.WNOHANG)
            if pid == 0:
                break
        except OSError:
            break


def goto(page, url):
    logger.info(f'Go to {url}')
    page.goto(url)
    page.wait_for_timeout(2333)


def make_page(browser, proxy):
    if proxy:
        context = browser.new_context(proxy=proxy.credentials)
    else:
        context = browser.new_context()
    page = context.new_page()
    page.set_viewport_size({'width': 1920, 'height': 1080})
    stealth_sync(page)
    return page


def is_phone_protected(page):
    if page.query_selector(PROMPT_MESSAGE_SEL):
        return True


def login_with_acc(page, acc):
    try:
        page.query_selector(LOGIN_BUTTON_SEL).click()
        page.wait_for_load_state('networkidle', timeout=60000)
        page.wait_for_selector('form[data-testid="login-form"]', timeout=30000)
        form = page.query_selector('form[data-testid="login-form"]')
        inputs = form.query_selector_all('input')

        logger.info('Inputing email')
        inputs[0].type(acc.login, delay=18, timeout=3345)
        logger.info('Inputing password')
        inputs[1].type(acc.password, delay=27, timeout=4533)
        page.wait_for_timeout(3777)
        page.keyboard.press('Enter')
        page.wait_for_timeout(4777)
        page.wait_for_load_state('domcontentloaded', timeout=60000)
    except Exception as e:
        logger.error(e)


def save_cookies(page, account):
    account.cookies = json.dumps(page.context.cookies())
    logger.info('Cookies saved')


def get_phone(page, tries=3):
    try:
        page.wait_for_selector(PHONE_BUTTON_SEL)
        phone_button = page.query_selector(PHONE_BUTTON_SEL)
        phone_button.click()
        page.wait_for_load_state('networkidle', timeout=30000)
        page.wait_for_timeout(2333)
        if is_captcha(page):
            raise exc.CaptchaFound()
        other_contact = page.query_selector(OTHER_BUTTON_SEL)
        phones = []
        phones.append(
            phone_button.query_selector('a').get_attribute('href').
            split(':')[-1]
        )
        if other_contact:
            other_contact = other_contact.query_selector_all('a')
            phones.extend([
                contact.get_attribute('href').split(':')[-1]
                for contact in other_contact if contact
            ])

        page.wait_for_timeout(2333)
        if any(phones):
            return '\n'.join(phones)
        if tries:
            page.reload()
            return get_phone(page, tries=tries-1)
    except exc.CaptchaFound:
        raise exc.CaptchaFound()
    except Exception as e:
        logger.error(e)
        if tries:
            page.reload()
            return get_phone(page, tries=tries-1)


def is_captcha(page):
    if page.query_selector(CAPTCHA_SEL):
        return True
