import re


def get_patterns():
    with open('address_patterns.txt', 'r') as file:
        return [
            re.compile(r'{}'.format(pattern.replace('\n', '')), re.IGNORECASE)
            for pattern in file.readlines()
        ]


def parse_address(text):
    for pattern in get_patterns():
        res = pattern.findall(text)
        if res:
            address = res[0][0]
            if address.isalnum() or len(address) < 40:
                return address.strip(',.\n ')
