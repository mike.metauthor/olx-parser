import csv

from sqlalchemy.orm import Session
from db.base import engine
from db.models import Proxy, Account
from utils.log import get_logger
import utils.exceptions as exc


logger = get_logger(__name__)


def import_proxies(filename='proxies_and_accs.csv'):
    with open(filename, 'r') as file:
        reader = csv.reader(file, delimiter=':')
        header = next(reader)
        rows = [
            {col: row[i] for i, col in enumerate(header)} for row in reader
        ]

    with Session(engine) as session:
        for row in rows:
            proxy = make_proxy(session, row)
            try:
                account = make_account(session, row, proxy)
                logger.info('Added new acc {} with proxy {}:{}'.format(
                    account.login, proxy.ip, proxy.port
                ))
            except exc.AccountExisted as e:
                logger.warn(e)

    logger.info('Proxies imported from {}'.format(filename))


def make_proxy(session, row):
    proxy = session.query(Proxy).filter_by(
        ip=row['ip'], port=row['port']
    ).first()
    if proxy:
        return proxy

    proxy = Proxy(
        ip=row['ip'], port=row['port'], login=row['login'],
        password=row['password'],
    )
    session.add(proxy)
    session.commit()
    session.refresh(proxy)
    return proxy


def make_account(session, row, proxy):
    acc = session.query(Account).filter_by(
        proxy_id=proxy.id
    ).first()
    if acc:
        raise exc.AccountExisted(acc.login)

    acc = Account(
        login=row['olx_login'], password=row['olx_password'],
        proxy_id=proxy.id,
    )
    session.add(acc)
    session.commit()
    session.refresh(acc)
    return acc
