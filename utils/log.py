import logging.config
import yaml
import os
import dotenv


dotenv.load_dotenv()


def _format_cfg(cfg):
    for opts in cfg['handlers'].values():
        if filename := opts.get('filename'):
            opts['filename'] = ''.join(filename)
    level = cfg['root'].get('level')
    cfg['root']['handlers'] = cfg['root']['handlers'].get(level)
    for handler in cfg['handlers'].values():
        if 'filename' in handler:
            handler['filename'] = handler['filename'].format(
                BASE_DIR=os.getcwd()
            )
    return cfg


def get_logger(name):
    path = os.path.join(os.path.dirname(__file__), 'logger.yml')
    with open(path, 'r') as yml_file:
        log_cfg = yaml.safe_load(yml_file.read())
        log_cfg = _format_cfg(log_cfg)
        logging.config.dictConfig(log_cfg)

    return logging.getLogger(name)
