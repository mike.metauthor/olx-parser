import datetime
import xml.etree.ElementTree as ET
from sqlalchemy.orm import Session
from db.base import engine
from db.models import XMLFile


def preprocess():
    root = ET.Element('listings', type="items-import-export")
    title = ET.Element('title')
    title.text = 'akra.com.ua'
    url = ET.Element('url')
    url.text = 'https://akra.com.ua'
    locale = ET.Element('locale')
    locale.text = 'uk'
    root.extend([title, url, locale])
    return root


def export_to_xml(data):
    root = preprocess()
    items = ET.Element('items')
    with Session(engine) as session:
        for offer in data:
            session.add(offer)
            items.append(offer.repr_xml)

    root.append(items)

    if data:
        tree = ET.ElementTree(root)
        date = datetime.datetime.now().date()
        filename = 'collected/{}_{}.xml'.format(date, offer.category_id)

        tree.write(
            filename,
            xml_declaration=True,
            encoding='utf-8',
            method='xml',
        )

        with Session(engine) as session:
            row = XMLFile(
                filename=filename,
                date=date,
                category=data[0].category_id,
                amount=len(data),
                first_offer_id=data[0].id,
                last_offer_id=data[-1].id,
            )
            session.add(row)
            session.commit()
