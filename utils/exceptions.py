class ParserError(Exception):
    pass


class OlxApiNotResponded(ParserError):
    def __init__(self, code, error) -> None:
        self.code = code
        self.error = error

    def __str__(self) -> str:
        return '[{}]Bad answer from server {}'.format(self.code, self.error)


class CaptchaFound(ParserError):
    def __str__(self):
        return 'Captcha found'


class AccountExisted(ParserError):
    def __init__(self, login) -> None:
        self.login = login

    def __str__(self) -> str:
        return '{} already existed'.format(self.login)
