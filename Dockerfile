FROM python:3.11
ENV PARSER_NAME=$PARSER_NAME

WORKDIR /usr/src/app/"${PARSER_NAME:-parser}"

COPY requirements.txt /usr/src/app/"${PARSER_NAME:-parser}"
RUN pip install -r /usr/src/app/"${PARSER_NAME:-parser}"/requirements.txt
RUN playwright install chromium
RUN playwright install-deps chromium
COPY . /usr/src/app/"${PARSER_NAME:-parser}"
