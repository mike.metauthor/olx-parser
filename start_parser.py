from db.base import connect_to_db, add_city_codes
from db.models import City
from sqlalchemy import func
from utils.log import get_logger
from parsers.api_olx_parser import parse_all_api_urls
from utils.proxies import import_proxies

logger = get_logger(__name__)


def check_db():
    with connect_to_db() as session:
        cities = session.query(func.count()).select_from(City).scalar()

    if cities < 9280:
        add_city_codes()

    import_proxies()


if __name__ == '__main__':
    check_db()
    logger.info('Parser started')
    parse_all_api_urls()
    logger.info('Finished')
