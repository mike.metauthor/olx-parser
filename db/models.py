import datetime
from sqlalchemy import ForeignKey
from db.base import Base
from sqlalchemy.orm import Mapped, mapped_column, relationship
from db.enums import Category, Currency, Rooms, Heating
import xml.etree.ElementTree as ET


class Contact(Base):
    __tablename__ = 'contacts'

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str]
    phones: Mapped[str] = mapped_column(default='')

    @property
    def repr_xml(self):
        contacts = ET.Element('contacts')
        phones = ET.Element('phones')

        for phone in self.phones.split('\n'):
            el = ET.Element('phone')
            el.text = phone
            phones.append(el)

        name = ET.Element('name')
        name.text = self.name
        contacts.append(name)
        contacts.append(phones)

        return contacts


class City(Base):
    __tablename__ = 'bff_regions'

    id: Mapped[int] = mapped_column(primary_key=True)
    geoname_id: Mapped[int] = mapped_column(default=0)
    pid: Mapped[int] = mapped_column(nullable=False)
    numleft: Mapped[int] = mapped_column(nullable=False)
    numright: Mapped[int] = mapped_column(nullable=False)
    numlevel: Mapped[int] = mapped_column(nullable=False)
    title_en: Mapped[str] = mapped_column(default='')
    title_ru: Mapped[str] = mapped_column(default='')
    title_uk: Mapped[str] = mapped_column(default='')
    title_alt: Mapped[str] = mapped_column(nullable=False)
    declension: Mapped[str] = mapped_column(nullable=False)
    keyword: Mapped[str] = mapped_column(default='', nullable=False)
    ycoords: Mapped[str] = mapped_column(default='', nullable=False)
    enabled: Mapped[int] = mapped_column(nullable=False)
    city: Mapped[int] = mapped_column(nullable=False)
    fav: Mapped[int] = mapped_column(default=0)
    extra: Mapped[str] = mapped_column(nullable=False)


class Geo(Base):
    __tablename__ = 'geo'

    id: Mapped[int] = mapped_column(primary_key=True)
    city_id: Mapped[int] = mapped_column(
        ForeignKey(City.id), default=None, nullable=True
    )
    city: Mapped[City] = relationship()

    region: Mapped[str] = mapped_column(default='')
    address: Mapped[str] = mapped_column(default='')
    lat: Mapped[float] = mapped_column(nullable=True, default=None)
    lon: Mapped[float] = mapped_column(nullable=True, default=None)

    @property
    def repr_xml(self):
        geo = ET.Element('geo')
        geo.extend([
            city := ET.Element(
                'city', id=(str(self.city.id) if self.city else '-1')
            ),
            addr := ET.Element('addr'),
            lat := ET.Element('lat'),
            lon := ET.Element('lon'),
        ])
        if self.city:
            city.text = self.city.title_uk
        addr.text = self.address
        lat.text = str(self.lat)
        lon.text = str(self.lon)
        return geo


class OfferDetails(Base):
    __tablename__ = 'offer_details'

    id: Mapped[int] = mapped_column(primary_key=True)
    rooms: Mapped[int]
    area: Mapped[float] = mapped_column(nullable=True, default=None)
    living_area: Mapped[float] = mapped_column(nullable=True, default=None)
    floor: Mapped[int] = mapped_column(nullable=True, default=None)
    total_floors: Mapped[int] = mapped_column(nullable=True, default=None)
    living_complex: Mapped[str] = mapped_column(default='')
    heating: Mapped[int] = mapped_column(nullable=True, default=None)

    @property
    def repr_xml(self):
        params = ET.Element('params')
        params.append(rooms := ET.Element(
            'param', field='1', type='6', title='Кімнат',
            value=str(getattr(Rooms, '_' + str(self.rooms)).value),
        ))
        rooms.text = str(self.rooms).replace('5', '5+')

        if self.area:
            params.append(area := ET.Element(
                'param', field='2', type='10', value='0',
                title='Загальна площа',
            ))
            area.text = str(self.area)
        if self.floor:
            params.append(floor := ET.Element(
                'param', field='5', type='10', value='0', title='Поверх',
            ))
            floor.text = str(self.floor)
        if self.total_floors:
            params.append(total_floors := ET.Element(
                'param', field='6', type='10', value='0', title='Поверховість',
            ))
            total_floors.text = str(self.total_floors)
        if self.living_complex:
            params.append(living_complex := ET.Element(
                'param', field='21', type='1', value='0', title='Назва ЖК',
            ))
            living_complex.text = str(self.living_complex)
        if self.heating:
            params.append(ET.Element(
                'param', field='4', type='8', value=str(self.heating),
                title='Опалення',
            ))

        return params


class Offer(Base):
    __tablename__ = 'offers'

    id: Mapped[int] = mapped_column(primary_key=True)
    olx_id: Mapped[int] = mapped_column(unique=True)
    url: Mapped[str]

    title: Mapped[str] = mapped_column(default='')
    descr: Mapped[str] = mapped_column(default='')
    category_id: Mapped[int]

    geo_id: Mapped[int] = mapped_column(
        ForeignKey(Geo.id), nullable=True, default=None
    )
    geo: Mapped[Geo] = relationship()

    price: Mapped[float] = mapped_column(nullable=True, default=None)
    currency: Mapped[int] = mapped_column(default=Currency.UAH.value)

    contact_id: Mapped[int] = mapped_column(ForeignKey(Contact.id))
    contact: Mapped[Contact] = relationship()

    images: Mapped[str] = mapped_column(default='')
    params_id: Mapped[int] = mapped_column(ForeignKey(OfferDetails.id))
    params: Mapped[OfferDetails] = relationship()

    @property
    def repr_xml(self):
        params = self.params.repr_xml
        params.append(olx_id := ET.Element(
            'param', field='22', type='1', value='0', title='ID',
        ))
        olx_id.text = str(self.olx_id)
        images = ET.Element('images')
        if self.images:
            for photo in self.images.split('\n'):
                images.append(image := ET.Element('image'))
                image.text = photo

        offer = ET.Element('item', external='0', publicated='true')
        offer.extend([
            title := ET.Element('title'),
            descr := ET.Element('description'),
            category := ET.Element('category', title='Квартир'),
            self.geo.repr_xml,
            price := ET.Element(
                'price', currency=str(self.currency), free='0', exchange='0',
                mod='0', agreed='0',
            ),
            images,
            self.contact.repr_xml,
            params,
        ])

        title.text = self.title
        descr.text = self.descr
        category.text = str(self.category_id)
        price.text = str(self.price)
        return offer


class XMLFile(Base):
    __tablename__ = 'xml_files'

    id: Mapped[int] = mapped_column(primary_key=True)
    filename: Mapped[str]
    date: Mapped[datetime.date]
    category: Mapped[int]
    amount: Mapped[int]

    first_offer_id: Mapped[int] = mapped_column(ForeignKey(Offer.id))
    last_offer_id: Mapped[int] = mapped_column(ForeignKey(Offer.id))


class Proxy(Base):
    __tablename__ = 'proxies'

    id: Mapped[int] = mapped_column(primary_key=True)
    ip: Mapped[str]
    port: Mapped[int]
    login: Mapped[str]
    password: Mapped[str]

    @property
    def credentials(self):
        return {
            'server': f'http://{self.ip}:{self.port}',
            'username': self.login,
            'password': self.password
        }


class Account(Base):
    __tablename__ = 'accounts'

    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str]
    password: Mapped[str]
    proxy_id: Mapped[int] = mapped_column(ForeignKey(Proxy.id))
    proxy: Mapped[Proxy] = relationship()
    cookies: Mapped[str] = mapped_column(default='')
    last_used: Mapped[datetime.datetime] = mapped_column(
        default=datetime.datetime.now
    )
    captched_until: Mapped[datetime.datetime] = mapped_column(
        default=datetime.datetime.now
    )
