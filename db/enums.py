from enum import Enum


class Currency(Enum):
    UAH = 1
    USD = 3
    EUR = 4


class Category(Enum):
    rent = 3025
    sell = 3019


class Heating(Enum):
    centralized = 1
    own_boiler_house = 2
    individual_gas = 3
    individual_electro = 4
    solid_fuel = 5
    other = 6


class Rooms(Enum):
    _1 = 8
    _2 = 2
    _3 = 3
    _4 = 4
    _5 = 5
