import logging
from sqlalchemy import create_engine, text
from sqlalchemy.orm import Session
from sqlalchemy.orm import DeclarativeBase
from dotenv import load_dotenv
import pandas as pd
import os


load_dotenv()
engine = create_engine(os.environ['DB_CONN_URL'], echo=False)


class Base(DeclarativeBase):
    pass


def add_city_codes():
    df = pd.read_csv('raw/bff_regions.csv')
    df = df.fillna('')
    df.to_sql('bff_regions', engine, if_exists='append', index=False)


def connect_to_db():
    Base.metadata.create_all(engine)
    return Session(engine)
